const express = require("express")
const dotenv = require("dotenv")
const cors = require("cors")
const bodyParser = require("body-parser")

dotenv.config()
const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())
app.use(express.json())

// router
const router = require("./src/router/index")
const authRouter = require("./src/router/auth.router")
app.use("/api/v1", authRouter)
app.use("/api/v1", router)

const PORT = process.env.PORT

app.listen(PORT, () => {
  console.log(`Server running on https://localhost: ${PORT}`)
})
