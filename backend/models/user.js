"use strict"
const { Model } = require("sequelize")
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      User.belongsTo(models.Role, {
        foreignKey: "role_id",
      })
      User.hasMany(models.Chat, {
        foreignKey: "sender_id",
      })
      User.hasMany(models.Chat, {
        foreignKey: "receiver_id",
      })
    }
  }
  User.init(
    {
      role_id: DataTypes.STRING,
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      alamat: DataTypes.STRING,
      password: DataTypes.STRING,
      jenis_kelamin: DataTypes.STRING,
      image_url: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  )
  return User
}
