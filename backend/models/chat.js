"use strict"
const { Model } = require("sequelize")
module.exports = (sequelize, DataTypes) => {
  class Chat extends Model {
    static associate(models) {
      Chat.belongsTo(models.User, {
        foreignKey: "sender_id",
      })
      Chat.belongsTo(models.User, {
        foreignKey: "receiver_id",
      })
    }
  }
  Chat.init(
    {
      sender_id: DataTypes.STRING,
      receiver_id: DataTypes.STRING,
      message: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Chat",
    }
  )
  return Chat
}
