"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Users", [
      {
        id: "U-0001",
        role_id: "1",
        name: "Deddy Coral",
        email: "uhuy@gmail.com",
        alamat: "jl. jalan-jalan",
        password: "deddy123",
        jenis_kelamin: "Laki - laki",
        image_url: null,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "U-0002",
        role_id: "2",
        name: "Putri",
        email: "ahay@gmail.com",
        alamat: "jl. jalan",
        password: "deddy123",
        jenis_kelamin: "Perempuan",
        image_url: null,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Users")
  },
}
