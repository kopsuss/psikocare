"use strict"

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Chats", [
      {
        id: "C-0001",
        sender_id: "U-0001",
        receiver_id: "U-0002",
        message: "Hi Pasien",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "C-0002",
        sender_id: "U-0002",
        receiver_id: "U-0001",
        message: "Hi Dokter",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ])
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Chats")
  },
}
