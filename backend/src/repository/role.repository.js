const { where } = require("sequelize")
const models = require("../../models/")
const Role = models.Role

const findRole = async () => {
  const allRole = await Role.findAll()
  return allRole
}

const findRoleById = async (id) => {
  const role = await Role.findOne({
    where: {
      id,
    },
  })

  return role
}

module.exports = { findRole, findRoleById }
