const models = require("../../models/")
const User = models.User
const bcrypt = require("bcrypt")
const { Op } = require("sequelize")

const findUser = async () => {
  const userAll = await User.findAll()
  return userAll
}

const findUserDokter = async (page, size) => {
  const offset = (page - 1) * size
  const dokterAll = await User.findAll({
    where: {
      role_id: {
        [Op.ne]: "2",
      },
    },
  })
  const dataLength = dokterAll.length
  const dokter = await User.findAll({
    where: {
      role_id: {
        [Op.ne]: "2",
      },
    },
    offset: offset,
    limit: size,
    order: [["id", "DESC"]],
    order: [["id", "DESC"]],
  })

  return { dokter, dataLength }
}

const findUserPasien = async () => {
  const pasienAll = await User.findAll({
    where: {
      role_id: {
        [Op.ne]: "1",
      },
    },
  })
  return pasienAll
}

const findUserById = async (id) => {
  const user = await User.findOne({
    where: {
      id,
    },
  })

  return user
}

const findUserByEmail = async (email) => {
  const user = await User.findOne({
    where: {
      email,
    },
  })

  return user
}

const generateNewId = (existingIds) => {
  const maxNumber = existingIds.reduce((max, id) => {
    const currentNumber = parseInt(id.split("-")[1], 10)
    return currentNumber > max ? currentNumber : max
  }, 0)

  const newNumber = maxNumber + 1
  const newId = `U-${String(newNumber).padStart(4, "0")}`

  return newId
}

const createUser = async (userData) => {
  try {
    const existingIds = await User.findAll({ attributes: ["id"] })
    const newId = generateNewId(existingIds.map((user) => user.id))
    const hashedPassword = await bcrypt.hash(userData.password, 10)

    const user = await User.create({
      id: newId,
      role_id: userData.role_id,
      name: userData.name,
      email: userData.email,
      alamat: userData.alamat,
      password: hashedPassword,
      jenis_kelamin: userData.jenis_kelamin,
      tanggal_lahir: userData.tanggal_lahir,
      image_url: userData.image_url,
    })

    return user
  } catch (err) {
    throw err
  }
}

const editUser = async (id, updated) => {
  try {
    const {
      name,
      email,
      password: newPassword,
      image_url,
      jenis_kelamin,
      tanggal_lahir,
    } = updated

    const user = await User.findByPk(id)
    if (!user) throw new Error("user not found")

    const updatedData = {}

    if (name !== undefined) {
      updatedData.name = name
    }

    if (email !== undefined && email !== user.email) {
      updatedData.email = email
    }

    if (newPassword !== undefined) {
      const hashedPassword = await bcrypt.hash(newPassword, 10)
      updatedData.password = hashedPassword
    }

    if (image_url !== undefined) {
      updatedData.image_url = image_url
    }

    if (jenis_kelamin !== undefined) {
      updatedData.jenis_kelamin = jenis_kelamin
    }

    if (tanggal_lahir !== undefined) {
      updatedData.tanggal_lahir = tanggal_lahir
    }

    // no change to update
    if (Object.keys(updatedData).length === 0) return user

    const [, [updatedUser]] = await User.update(updatedData, {
      where: {
        id: id,
      },
      returning: true,
    })

    return updatedUser
  } catch (err) {
    console.error("Error during user update:", err.message)
    throw new Error("failed to update user")
  }
}

const deleteUser = async (id) => {
  const user = await User.destroy({
    where: {
      id,
    },
  })
  return user
}

const uploadUserPhoto = async (id, image_url) => {
  try {
    const updateUser = await User.update(
      { image_url: image_url },
      { where: { id }, returning: true }
    )

    if (updateUser[0] === 0) return null

    return updateUser[1][0].dataValues
  } catch (err) {
    throw err
  }
}

module.exports = {
  findUser,
  findUserDokter,
  findUserPasien,
  findUserById,
  findUserByEmail,
  createUser,
  deleteUser,
  editUser,
  uploadUserPhoto,
}
