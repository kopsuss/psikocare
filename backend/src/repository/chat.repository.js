const { where } = require("sequelize")
const models = require("../../models/")
const Chat = models.Chat
const User = models.User

const findChat = async () => {
  const allChat = await Chat.findAll({
    include: [
      {
        model: User,
      },
    ],
  })
  return allChat
}

const generateNewId = (existingIds) => {
  const maxNumber = existingIds.reduce((max, id) => {
    const currentNumber = parseInt(id.split("-")[1], 10)
    return currentNumber > max ? currentNumber : max
  }, 0)

  const newNumber = maxNumber + 1
  const newId = `C-${String(newNumber).padStart(4, "0")}`

  return newId
}

const createChat = async (chatData) => {
  try {
    const existingIds = await Chat.findAll({ attributes: ["id"] })
    const newId = generateNewId(existingIds.map((chat) => chat.id))

    const chat = await Chat.create({
      id: newId,
      message: chatData.message,
      sender_id: chatData.sender_id,
      receiver_id: chatData.receiver_id,
    })

    return chat
  } catch (err) {
    throw err
  }
}

const findChatBySenderId = async (sender_id) => {
  const chat = await Chat.findAll({
    include: [
      {
        model: User,
      },
    ],
    where: {
      sender_id,
    },
  })

  return chat
}

const findChatByReceiverId = async (receiver_id) => {
  const chat = await Chat.findAll({
    include: [
      {
        model: User,
      },
    ],
    where: {
      receiver_id,
    },
  })

  return chat
}

const deleteChat = async (id) => {
  const chat = await Chat.destroy({
    where: {
      id,
    },
  })
  return chat
}

module.exports = {
  findChat,
  createChat,
  findChatBySenderId,
  findChatByReceiverId,
  deleteChat,
}
