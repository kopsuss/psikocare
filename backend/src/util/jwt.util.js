const jwt = require("jsonwebtoken")
const dotenv = require("dotenv")
dotenv.config()

const blacklistedTokens = new Set()

const encodedToken = ({ user }) => {
  const token = jwt.sign(
    {
      id: user.id,
      role: user.role_id,
      name: user.name,
      email: user.email,
      alamat: user.alamat,
      password: user.password,
      jenis_kelamin: user.jenis_kelamin,
      tanggal_lahir: user.tanggal_lahir,
      riwayat: user.riwayat,
      image_url: user.image_url,
    },
    process.env.SECRET_KEY
  )

  id = user.id
  role = user.role_id
  name = user.name
  image_url = user.image_url

  return { token, id, role, name, image_url }
}

const decodeToken = (token) => {
  const decoded = jwt.verify(token, process.env.SECRET_KEY)

  return decoded
}

const isTokenBlacklisted = (token) => {
  return blacklistedTokens.has(token)
}

const blacklistToken = (token) => {
  blacklistedTokens.add(token)
}

module.exports = {
  encodedToken,
  decodeToken,
  isTokenBlacklisted,
  blacklistToken,
}
