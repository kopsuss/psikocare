const express = require("express")
const route = express.Router()
const roleController = require("../controller/role.controller")

route.get("/", roleController.allRole)
route.get("/:id", roleController.roleById)

module.exports = route
