// router
const express = require("express")
const router = express.Router()
const userRouter = require("./user.router")
const roleRouter = require("./role.router")
const chatRouter = require("./chat.router")
const authMiddleware = require("../middleware/auth.middleware")

// Terapkan middleware authenticateToken hanya pada route yang dilindungi
// router.use(authMiddleware.authenticateToken)
router.use("/role", roleRouter)
router.use("/user", userRouter)
router.use("/chat", chatRouter)

module.exports = router
