const express = require("express")
const route = express.Router()
const chatController = require("../controller/chat.controller")

route.get("/", chatController.allChat)
route.post("/sender-chat", chatController.postChat)
route.get("/sender-chat/:id", chatController.chatBySenderById)
route.get("/receiver-chat/:id", chatController.chatByReceiverById)
route.delete("/:id", chatController.deleteChat)

module.exports = route
