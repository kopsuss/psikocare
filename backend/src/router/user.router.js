const express = require("express")
const route = express.Router()
const userController = require("../controller/user.controller")
const { upload, uploadDir } = require("../../config/multer.user.config")

route.get("/", userController.allUsers)
route.get("/dokter", userController.allDokter)
route.get("/pasien", userController.allPasien)
route.get("/id/:id", userController.userById)
route.get("/email/:email", userController.userByEmail)
route.put("/id/:id", userController.updateUser)
route.delete("/id/:id", userController.removeUser)
route.post(
  "/upload/:id",
  upload.single("image_url"),
  userController.uploadUserPhoto
)
route.use("/upload", express.static(uploadDir))

module.exports = route
