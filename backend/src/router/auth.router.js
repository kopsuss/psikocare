// authRouter
const express = require("express")
const route = express.Router()
const authController = require("../controller/auth.controller")
const userController = require("../controller/user.controller")

route.post("/register", userController.postUser)
route.post("/login", authController.userLogin)
route.delete("/logout", authController.userLogout)

module.exports = route
