const {
  findUser,
  findUserDokter,
  findUserPasien,
  findUserById,
  findUserByEmail,
  createUser,
  deleteUser,
  editUser,
  uploadUserPhoto,
} = require("../repository/user.repository")

const jwt = require("../util/jwt.util")

const getAllUsers = async () => {
  const users = await findUser()
  return users
}

const getAllDokter = async (page, size) => {
  const dokter = await findUserDokter(page, size)
  return dokter
}

const getAllPasien = async () => {
  const pasien = await findUserPasien()
  return pasien
}

const getUserById = async (id) => {
  const user = await findUserById(id)
  return user
}

const getUserByEmail = async (email) => {
  const user = await findUserByEmail(email)
  return user
}

const insertUser = async (userData) => {
  const userEmail = await findUserByEmail(userData.email)

  if (userEmail) {
    throw new Error("Email already exists!")
  }

  const user = await createUser(userData)
  const token = jwt.encodedToken({ user })

  return { user, token }
}

const deleteUserById = async (id) => {
  const user = await deleteUser(id)
  return user
}

const editUserById = async (id, newUser) => {
  try {
    const user = await getUserById(id)
    if (!user) throw new Error("User not found")

    const userEmail = await findUserByEmail(newUser.email)
    if (userEmail && userEmail.id !== id) throw new Error("Email already added")

    const updatedUser = await editUser(id, newUser)
    return updatedUser
  } catch (err) {
    throw new Error(err.message)
  }
}

const updateUserPhoto = async (id, image_url) => {
  try {
    return await uploadUserPhoto(id, image_url)
  } catch (err) {
    throw err
  }
}

module.exports = {
  getAllUsers,
  getAllDokter,
  getAllPasien,
  getUserById,
  getUserByEmail,
  insertUser,
  editUserById,
  deleteUserById,
  updateUserPhoto,
}
