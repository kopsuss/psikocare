const {
  findChat,
  createChat,
  findChatBySenderId,
  findChatByReceiverId,
  deleteChat,
} = require("../repository/chat.repository")

const getAllChat = async () => {
  const chats = await findChat()
  return chats
}

const insertChat = async (newChat) => {
  try {
    const chat = await createChat(newChat)
    return chat
  } catch (err) {
    throw err
  }
}

const getChatBySenderId = async (sender_id) => {
  const chat = await findChatBySenderId(sender_id)
  return chat
}

const getChatByReceiverId = async (receiver_id) => {
  const chat = await findChatByReceiverId(receiver_id)
  return chat
}

const deleteChatById = async (id) => {
  const chat = await deleteChat(id)
  return chat
}

module.exports = {
  getAllChat,
  insertChat,
  getChatBySenderId,
  getChatByReceiverId,
  deleteChatById,
}
