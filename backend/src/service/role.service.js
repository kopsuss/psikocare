const { findRole, findRoleById } = require("../repository/role.repository")

const getAllRole = async () => {
  const roles = await findRole()
  return roles
}

const getRoleById = async (id) => {
  const role = await findRoleById(id)
  return role
}

module.exports = { getAllRole, getRoleById }
