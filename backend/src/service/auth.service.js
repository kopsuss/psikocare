const { findUserByEmail } = require("../repository/user.repository")
const bcrypt = require("bcrypt")
const jwtUtil = require("../util/jwt.util")

const login = async (userData) => {
  const user = await findUserByEmail(userData.email)
  if (!user) {
    const error = new Error("Invalid email")
    error.status = 400
    throw error
  }

  const passwordMatch = await bcrypt.compare(userData.password, user.password)
  if (!passwordMatch) {
    const error = new Error("Invalid password")
    error.status = 400
    throw error
  }

  const result = jwtUtil.encodedToken({ user })
  return result
}

const logout = async (token) => {
  if (!token) {
    const error = new Error("Token Missing !")
    error.status = 400
    throw error
  }

  jwtUtil.blacklistToken(token)
}

module.exports = { login, logout }
