const {
  getAllUsers,
  getAllDokter,
  getAllPasien,
  getUserById,
  getUserByEmail,
  insertUser,
  editUserById,
  deleteUserById,
  updateUserPhoto,
} = require("../service/user.service")

const allUsers = async (req, res) => {
  try {
    const users = await getAllUsers()
    res.status(200).json({ data: users })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const allDokter = async (req, res) => {
  const page = req.query.page || 1
  const size = req.query.size || 10
  try {
    const { dokter, dataLength } = await getAllDokter(page, size)
    res.status(200).json({
      data: dokter,
      totalItems: dataLength,
      currentPage: parseInt(page),
      totalPages: Math.ceil(dataLength / size),
    })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const allPasien = async (req, res) => {
  try {
    const pasien = await getAllPasien()
    res.status(200).json({ data: pasien })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const userById = async (req, res) => {
  try {
    const userId = req.params.id
    const user = await getUserById(userId)

    if (!user) return res.status(404).json({ message: `user not found` })

    res.status(200).json({ data: user })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const userByEmail = async (req, res) => {
  try {
    const userEmail = req.params.email
    const user = await getUserByEmail(userEmail)

    if (!user) return res.status(404).json({ message: "user not found" })

    res.status(200).json({ data: user })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const postUser = async (req, res) => {
  try {
    const newUserData = req.body
    const { user, token } = await insertUser(newUserData)

    res.status(201).json({
      data: { user, token: token.token },
      message: "Sign Up Successful!",
    })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const removeUser = async (req, res) => {
  try {
    const userId = req.params.id
    await deleteUserById(userId)

    res.status(200).json({ message: "Successfull Delete User" })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const updateUser = async (req, res) => {
  const id = req.params.id
  const userData = req.body

  if (!userData || Object.keys(userData).length === 0) {
    return res.status(400).json({ message: "data must have value" })
  }

  try {
    const user = await editUserById(id, userData)
    if (!user) return res.status(404).json({ message: "user not found" })

    res.status(200).json({
      data: user,
      message: "success update",
    })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const uploadUserPhoto = async (req, res) => {
  try {
    const id = req.params.id
    const user = await getUserById(id)
    if (!user) return res.status(404).json({ message: "user not found" })

    if (!req.file) return res.status(400).json({ message: "no file uploaded" })

    const image_url = req.file.filename
    const updateUser = await updateUserPhoto(id, image_url)

    res
      .status(201)
      .json({ message: "succesfull update user", data: updateUser })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

module.exports = {
  allUsers,
  allDokter,
  allPasien,
  userById,
  userByEmail,
  postUser,
  updateUser,
  removeUser,
  uploadUserPhoto,
}
