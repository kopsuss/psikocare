const { getAllRole, getRoleById } = require("../service/role.service")

const allRole = async (req, res) => {
  try {
    const role = await getAllRole()
    res.status(200).json({ data: role })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const roleById = async (req, res) => {
  try {
    const roleId = req.params.id
    const role = await getRoleById(roleId)

    if (!role) return res.status(200).json({ message: "role not found" })

    res.status(200).json({ data: role })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

module.exports = { allRole, roleById }
