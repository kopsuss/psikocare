const { login, logout } = require("../service/auth.service")

const userLogin = async (req, res) => {
  try {
    const userData = req.body
    const result = await login(userData)
    return res.status(200).json({
      message: "Login Successfull!",
      token: result.token,
      role: result.role,
      id: result.id,
      name: result.name,
    })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const userLogout = async (req, res) => {
  try {
    const authHeader =
      req.headers["Authorization"] || req.headers["authorization"]
    const token = authHeader && authHeader.split(" ")[1]
    await logout(token)
    return res.status(200).json({ message: "Logout Successfull!" })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

module.exports = { userLogin, userLogout }
