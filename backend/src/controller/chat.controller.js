const {
  getAllChat,
  insertChat,
  getChatBySenderId,
  getChatByReceiverId,
  deleteChatById,
} = require("../service/chat.service")

const allChat = async (req, res) => {
  try {
    const user = await getAllChat()
    res.status(200).json({ data: user })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const postChat = async (req, res) => {
  try {
    const newChatData = req.body

    const chat = await insertChat(newChatData)
    res.status(201).json({ data: chat, message: "berhasil menambahkan chat" })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const chatBySenderById = async (req, res) => {
  try {
    const senderId = req.params.id
    const sender = await getChatBySenderId(senderId)
    if (!sender) return res.status(404).json({ message: "sender not found" })

    res.status(200).json({ data: sender })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const chatByReceiverById = async (req, res) => {
  try {
    const receiverId = req.params.id
    const receiver = await getChatByReceiverId(receiverId)
    if (!receiver)
      return res.status(404).json({ message: "receiver not found" })

    res.status(200).json({ data: receiver })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

const deleteChat = async (req, res) => {
  try {
    const idChat = req.params.id
    const chat = await deleteChatById(idChat)
    if (!chat) return res.status(404).json({ message: "chat not found" })

    res.status(200).json({ message: "successfull delete chat" })
  } catch (err) {
    res.status(500).json({ message: err.message })
  }
}

module.exports = {
  allChat,
  postChat,
  chatBySenderById,
  chatByReceiverById,
  deleteChat,
}
