const Joi = require("joi")

const userAddSchema = Joi.object({
  role_id: Joi.number().required(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  alamat: Joi.string().required(),
  password: Joi.string().required(),
  jenis_kelamin: Joi.string().required(),
  tanggal_lahir: Joi.string().required(),
})

module.exports = userAddSchema
