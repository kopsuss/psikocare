const Auth = require("../repository/user.repository")
const jwtUtil = require("../util/jwt.util")

const authenticateToken = async (req, res, next) => {
  const authHeader =
    req.headers["authorization"] || req.headers["Authorization"]
  const token = authHeader && authHeader.split(" ")[1]

  if (!token)
    return res
      .status(404)
      .json({ message: "Unauthorized access: Token not provided" })

  try {
    const decodeToken = jwtUtil.decodeToken(token)
    const user = await Auth.findUserById(decodeToken.id)

    if (!user) return res.status(404).json({ message: "user not found" })

    req.user = user
    const role = decodeToken.role
    console.log(decodeToken)

    if (
      (role == 1 && role == 2 && req.path.startsWith("/role")) ||
      req.path.startsWith("/chat") ||
      req.path.startsWith("/user")
    ) {
      next()
    } else {
      return res.status(404).send("Forbidden")
    }

    return
  } catch (err) {
    res.status(404).json({ message: "Unathorized access" })
  }
}

module.exports = { authenticateToken }
