import { instance } from "../axios/index"

// user
async function getUser() {
  try {
    const res = await instance.get("/user")
    return res
  } catch (err) {
    throw new Error(console.log(err))
  }
}

// user by id
async function getUserById(id) {
  try {
    const res = await instance.get(`/user/id/${id}`)
    return res
  } catch (err) {
    throw err
  }
}

// edit User
async function editUser(id, data) {
  try {
    const res = await instance.put(`/user/id/${id}`, data)
    return res
  } catch (err) {
    throw err
  }
}

// uload image
async function uploadImage(id, formData) {
  try {
    const res = await instance.post(`/user/upload/${id}`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    return res
  } catch (err) {
    throw err
  }
}

// delete user
async function deleteUser(id) {
  try {
    const res = await instance.delete(`/user/id/${id}`)
    return res
  } catch (err) {
    throw err
  }
}

// dokter
async function getDokter(page, size) {
  const payload = {
    page: page,
    size: size,
  }
  try {
    const res = await instance.get("/user/dokter", { params: payload })
    return res
  } catch (err) {
    throw new Error(console.log(err))
  }
}

// pasien
async function getPasien() {
  try {
    const res = await instance.get("/user/pasien")
    return res
  } catch (err) {
    throw new Error(console.log(err))
  }
}

// register
async function register(data) {
  try {
    const res = await instance.post("/register", data)
    return res
  } catch (err) {
    throw err
  }
}

// login
async function login(data) {
  try {
    const res = await instance.post("/login", data)
    return res
  } catch (err) {
    throw err
  }
}

// log out
async function logout() {
  try {
    const res = await instance.delete("/logout")
    return res
  } catch (err) {
    throw err
  }
}

// all chat
async function getAllChat() {
  try {
    const res = await instance.get("/chat")
    return res
  } catch (err) {
    throw err
  }
}

// get receiver chat
async function getReceiverChat(id) {
  try {
    const res = await instance.get(`/chat/receiver-chat/${id}`)
    return res
  } catch (err) {
    throw err
  }
}

// get sender chat
async function getSenderChat(id) {
  try {
    const res = await instance.get(`/chat/sender-chat/${id}`)
    return res
  } catch (err) {
    throw err
  }
}

// post sender chat
async function postSenderChat(data) {
  try {
    const res = await instance.post("/chat/sender-chat", data)
    return res
  } catch (err) {
    throw err
  }
}

module.exports = {
  getUser,
  getUserById,
  editUser,
  uploadImage,
  deleteUser,
  getDokter,
  getPasien,
  register,
  login,
  logout,
  getAllChat,
  getReceiverChat,
  getSenderChat,
  postSenderChat,
}
