import React from "react"
import "./wraperKonsultasi.css"
import Image from "next/image"
import wraperImg from "@/../public/assets/wraper_konsultasi.svg"

const WraperKonsultasi = () => {
  return (
    <>
      <div className="wraperKonsultasi">
        <div className="wraperKonsultasi_desc">
          <p>Konsultasi dengan Psikolog ahli</p>
          <p>
            Berbincang lebih dekat tentang keluhan kepada dokter / psikiater
            ahli dibidangnya
          </p>
        </div>
        <div className="wraperKonsultasi_img">
          <Image src={wraperImg} alt="" width={0} height={0} />
        </div>
      </div>
    </>
  )
}

export default WraperKonsultasi
