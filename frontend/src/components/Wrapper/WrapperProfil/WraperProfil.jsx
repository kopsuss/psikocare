import React from "react"
import Image from "next/image"
import "./wraperProfil.css"
import Link from "next/link"
import { FaUserAlt } from "react-icons/fa"

const WraperProfil = ({ dataUser, handleDelete }) => {
  return (
    <div className="wraperProfil">
      <div className="wraperProfil_user">
        <div className="border-2 border-black wraperProfil_user_img">
          {dataUser?.image_url ? (
            <div className="w-full h-full">
              <Image
                src={`/upload/user/${dataUser?.image_url}`}
                alt={dataUser?.name}
                width={1000}
                height={1000}
              />
            </div>
          ) : (
            <FaUserAlt className="w-full h-full m-5" />
          )}
        </div>
        <div className="wraperProfil_user_desc">
          <div className="wraperProfil_user_desc_user">
            <p>{dataUser?.name}</p>
            <p>{dataUser?.email}</p>
          </div>
          <div className="wraperProfil_user_desc_place">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
              />
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
              />
            </svg>
            <p>{dataUser?.alamat}</p>
          </div>
        </div>
      </div>
      <div className="wraperProfil_user_action">
        <Link
          href={"editProfilePasien"}
          className="btn lg:btn-md btn-sm text-xs lg:text-sm"
        >
          Edit Profile
        </Link>
        <button
          onClick={handleDelete}
          className="btn lg:btn-md btn-sm text-xs lg:text-sm"
        >
          Delete Account
        </button>
      </div>
    </div>
  )
}

export default WraperProfil
