"use client"

import "./navbar.css"
import Image from "next/image"
import Link from "next/link"
import { useEffect, useRef, useState } from "react"
import Cookies from "js-cookie"
import { useRouter } from "next/navigation"
import Swal from "sweetalert2"

// components
import FormLogin from "../Form/FormLogin/FormLogin"
import ModalUser from "../modal/ModalUser/ModalUser"
import LiveChat from "../LiveChat/LiveChat"

// assets
import logo from "@/../public/assets/logo.svg"
import { usePathname } from "next/navigation"
import { RxHamburgerMenu } from "react-icons/rx"
import { FaUserAlt } from "react-icons/fa"

// api
import { login, getUserById } from "../../modules/fetch/index.js"

const Navbar = () => {
  const pathname = usePathname()
  const modalCheckbox = useRef(null)
  const [role, setRole] = useState(null)
  const [name, setName] = useState(null)
  const [modalUser, setModalUser] = useState(false)
  const [openHamburger, setOpenHamburger] = useState(false)
  const [dataUser, setDataUser] = useState(null)
  const router = useRouter()

  const fetchData = async () => {
    try {
      const id = Cookies.get("userId")
      // Pemeriksaan apakah id memiliki nilai sebelum melakukan request
      if (id) {
        const res = await getUserById(id)
        setDataUser(res.data.data)
      } else {
        null
      }
    } catch (err) {
      console.log(err.response.data.message)
    }
  }

  fetchData()

  const handleLogin = async (e) => {
    e.preventDefault()
    try {
      const res = await login({
        email: e.target.email.value,
        password: e.target.password.value,
      })

      Cookies.set("token", res.data.token, { expires: 1 })
      Cookies.set("role", res.data.role, { expires: 1 })
      Cookies.set("name", res.data.name, { expires: 1 })
      Cookies.set("userId", res.data.id, { expires: 1 })

      // Tetapkan peran dan nama state segera
      const role = res.data.role
      const name = res.data.name
      setRole(role)
      setName(name)

      modalCheckbox.current.checked = false
      e.target.email.value = ""
      e.target.password.value = ""

      router.push("/")
      router.refresh()
    } catch (err) {
      Swal.fire({
        icon: "error",
        title: err.response.data.message,
        showConfirmButton: false,
        timer: 3000,
        customClass: "swal-custom",
      })
    }
  }

  const handleOpenHamburger = () => {
    setOpenHamburger(!openHamburger)
  }

  const handleLogOut = () => {
    const fetchData = async () => {
      try {
        Cookies.remove("token")
        Cookies.remove("role")
        Cookies.remove("name")
        Cookies.remove("userId")
        setRole(null)
        setName(null)
        router.push("/")
      } catch (err) {
        console.log(err.response.message)
      }
    }

    fetchData()
  }

  useEffect(() => {
    const role = Cookies.get("role")
    const name = Cookies.get("name")
    setRole(role)
    setName(name)
  }, [])

  return (
    <div className="">
      <div className="navbar">
        <div className="navbar_item_hamburger" onClick={handleOpenHamburger}>
          <RxHamburgerMenu size={25} />
        </div>
        <div className="navbar_item">
          <div className="navbar_item_logo">
            <Image src={logo} alt="logo" width={100} height={100} />
          </div>
          <div className={`navbar_item_list ${openHamburger ? "active" : ""}`}>
            <Link className={`${pathname === "/" ? "active" : ""}`} href={"/"}>
              Beranda
            </Link>
            <Link
              className={
                pathname && pathname.startsWith("/edukasi") ? "active" : ""
              }
              href={"/edukasi"}
            >
              Edukasi
            </Link>
            <Link
              className={
                pathname && pathname.startsWith("/konsultasi") ? "active" : ""
              }
              href={"/konsultasi"}
            >
              Konsultasi
            </Link>
          </div>
        </div>
        {role != null ? (
          <>
            <div
              onClick={() => setModalUser(!modalUser)}
              className="navbar_user cursor-pointer"
            >
              <p>{name}</p>
              <div className="border shadow-sm navbar_user_img">
                {dataUser?.image_url ? (
                  <Image
                    src={`/upload/user/${dataUser.image_url}`}
                    alt="asu"
                    width={1000}
                    height={1000}
                  />
                ) : (
                  <FaUserAlt className="w-full h-full" />
                )}
              </div>
            </div>
            <LiveChat />
          </>
        ) : (
          <label htmlFor="openFormLogin" className="navbar_btn">
            Login
          </label>
        )}
      </div>
      <FormLogin
        openFormLogin="openFormLogin"
        handleLogin={handleLogin}
        modalCheckbox={modalCheckbox}
      />
      <div className="user">
        <ModalUser
          modalUser={modalUser}
          setModalUser={setModalUser}
          handleLogOut={handleLogOut}
          dataUser={dataUser}
        />
      </div>
    </div>
  )
}

export default Navbar
