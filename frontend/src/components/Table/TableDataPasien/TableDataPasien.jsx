import React from "react"
import "./tableDataPasien.css"
import Image from "next/image"

// assets
import { MdDelete } from "react-icons/md"
import { FaUserAlt } from "react-icons/fa"

const TableDataPasien = ({
  filterDataPasien,
  handleSearchTerm,
  searchTerm,
}) => {
  return (
    <div className="dataPasien">
      <div className="dataPasien_title">
        <p>Table Data Pasien</p>
        <div className="dataPasien_search">
          <input
            type="search"
            placeholder="search pasien"
            value={searchTerm}
            onChange={handleSearchTerm}
          />
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
            />
          </svg>
        </div>
      </div>

      <div className="tableDataPasien overflow-x-auto">
        <table>
          <thead>
            <tr>
              <th>No</th>
              <th>Profile</th>
              <th>Nama Pasien</th>
              <th>Email</th>
              <th>Jenis Kelamin</th>
            </tr>
          </thead>
          <tbody>
            {filterDataPasien.map((value, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>
                  <div className="w-9 h-9 bg-slate-400 rounded-full mx-auto overflow-hidden">
                    {value.image_url ? (
                      <Image
                        src={`/upload/user/${value.image_url}`}
                        alt="docter"
                        width={100}
                        height={100}
                      />
                    ) : (
                      <FaUserAlt className="w-full h-full" />
                    )}
                  </div>
                </td>
                <td>{value.name}</td>
                <td>{value.email}</td>
                <td>{value.jenis_kelamin}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default TableDataPasien
