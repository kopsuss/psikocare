import React from "react"
import "./tableRiwayatUser.css"
import Image from "next/image"

import { FaUserAlt } from "react-icons/fa"

const TableRiwayatUser = ({ riwayat }) => {
  const processedUserIds = new Set()

  return (
    <>
      <p className="riwayat">Riwayat</p>
      <div className="tableRiwayat overflow-x-auto">
        <table className="table text-center text-white">
          <thead>
            <tr className="text-white">
              <th>Pofile</th>
              <th>Nama Dokter</th>
              <th>Waktu Interaksi</th>
              <th>Kegiatan</th>
            </tr>
          </thead>
          <tbody>
            {riwayat.map((value) => {
              const userId = value.User.id

              // Cek apakah id user sudah diproses
              if (!processedUserIds.has(userId)) {
                processedUserIds.add(userId)

                // Mengonversi tanggal dan waktu dari UTC ke waktu lokal Indonesia
                const localDate = new Date(value.User.updatedAt).toLocaleString(
                  "id-ID"
                )

                return (
                  <tr key={value.id}>
                    <td>
                      <div className="w-10 h-10 bg-slate-400 rounded-full mx-auto overflow-hidden">
                        {value.User.image_url ? (
                          <Image
                            src={`/upload/user/${value.User.image_url}`}
                            alt="docter"
                            width={100}
                            height={100}
                          />
                        ) : (
                          <FaUserAlt className="w-full h-full" />
                        )}
                      </div>
                    </td>
                    <td>{value.User.name}</td>
                    <td>{localDate}</td>
                    <td>Konsultasi Online</td>
                  </tr>
                )
              }

              return null // Jika id user sudah diproses, return null
            })}
          </tbody>
        </table>
      </div>
    </>
  )
}

export default TableRiwayatUser
