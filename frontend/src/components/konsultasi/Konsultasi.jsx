"use client"

import React from "react"
import "./konsultasi.css"
import Image from "next/image"
// components
import ChatKonsultasiDokter from "./ChatKonsultasiDokter"

// asstes
import { FaUserAlt } from "react-icons/fa"

export const Konsultasi = ({
  handleSearch,
  filterDataPasien,
  handleOpenChat,
  openChat,
  chat,
  handleSenderChatDokter,
  detailPasien,
}) => {
  return (
    <div className="konsultasi">
      <p>Chat</p>
      <div className="konsultasi_search">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="w-6 h-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
          />
        </svg>
        <input
          type="search"
          onChange={handleSearch}
          placeholder="search or start a new chat"
        />
      </div>
      <div className="konsultasi_content">
        <div className="konsultasi_content_left">
          {filterDataPasien.map((value) => (
            <div
              key={value.id}
              className="konsultasi_content_left_user"
              onClick={() => handleOpenChat(value.id)}
            >
              <div className="border-2 border-black shadow-sm konsultasi_content_left_user_img">
                {value.image_url ? (
                  <Image
                    src={`/upload/user/${value.image_url}`}
                    alt=""
                    width={100}
                    height={100}
                  />
                ) : (
                  <FaUserAlt className="w-full h-full" />
                )}
              </div>
              <div className="konsultasi_content_left_user_desc">
                <p>{value.name}</p>
              </div>
            </div>
          ))}
        </div>

        <div className="konsultasi_content_right">
          <ChatKonsultasiDokter
            chat={chat}
            openChat={openChat}
            handleSenderChatDokter={handleSenderChatDokter}
            detailPasien={detailPasien}
          />
        </div>
      </div>
    </div>
  )
}
