import React from "react"
import Image from "next/image"

// asstes
import { FaUserAlt } from "react-icons/fa"
import Cookies from "js-cookie"

const ChatKonsultasiDokter = ({
  openChat,
  chat,
  handleSenderChatDokter,
  detailPasien,
}) => {
  return (
    <>
      {openChat ? (
        <>
          {/* {detailPasien.map((value) => ( */}
          <div className="chatKonsultasi_profile">
            <div className="chatKonsultasi_profile_img">
              {detailPasien?.image_url ? (
                <Image
                  src={`/upload/user/${detailPasien?.image_url}`}
                  alt=""
                  width={100}
                  height={100}
                />
              ) : (
                <FaUserAlt className="w-full h-full" />
              )}
            </div>
            <div className="chatKonsultasi_profile_desc">
              <p>{detailPasien?.name}</p>
            </div>
          </div>
          {/* ))} */}
          <div className="chatKonsultasi_chat">
            <div className="chatKonsultasi_chat_text">
              {chat.map((value, i) => (
                <div
                  key={i}
                  className={
                    value.sender_id === Cookies.get("userId")
                      ? "chatKonsultasi_chat_text_me"
                      : "chatKonsultasi_chat_text_you"
                  }
                >
                  <p>{value.message}</p>
                </div>
              ))}
            </div>
            <form onSubmit={handleSenderChatDokter}>
              <div className="chatKonsultasi_chat_input">
                <input
                  type="text"
                  name="chat"
                  placeholder="Type here"
                  className="input input-sm input-bordered w-full"
                />

                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-6 h-6 cursor-pointer"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z"
                  />
                </svg>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-6 h-6 cursor-pointer"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5"
                  />
                </svg>
              </div>
            </form>
          </div>
        </>
      ) : (
        <div className="objectChat">
          <p>Klick salah satu user</p>
        </div>
      )}
    </>
  )
}

export default ChatKonsultasiDokter
