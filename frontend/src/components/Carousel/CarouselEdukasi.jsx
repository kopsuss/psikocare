"use client"

import Image from "next/image"
import React, { useEffect, useRef, useState } from "react"
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react"

// Import Swiper styles
import "swiper/css"
import "swiper/css/pagination"
import "swiper/css/navigation"
// import required modules
import { Autoplay } from "swiper/modules"

// css
import "./carouselEduksai.css"

// assets
import edukasiCarousel from "@/../public/assets/carousel_edukasi.svg"

// api
import { getListEdukasi } from "@/modules/fetch/index"

const CarouselEdukasi = () => {
  const [dataListEdukasi, setDataListEdukasi] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getListEdukasi()
        setDataListEdukasi(res.data)
      } catch (err) {
        console.log(err)
      }
    }

    fetchData()
  }, [])

  return (
    <div className="edukasi">
      <div className="edukasi_title">
        <p>Edukasi</p>
      </div>
      <div className="edukasi_carousel">
        <p>Beragam Edukasi Untuk kita</p>
        <Swiper
          slidesPerView={1.5}
          spaceBetween={10}
          breakpoints={{
            1280: {
              slidesPerView: 5,
            },
            1024: {
              slidesPerView: 4,
            },
            768: {
              slidesPerView: 3,
              spaceBetween: 20,
            },
          }}
          loop={true}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          modules={[Autoplay]}
          className="mySwiper"
        >
          {dataListEdukasi.map((value) => (
            <SwiperSlide key={value.id}>
              <div className="edukasi_carousel_content">
                <div className="edukasi_carousel_content_img">
                  <Image
                    src={edukasiCarousel}
                    alt="edukasi carousel"
                    width={100}
                    height={100}
                  />
                </div>
                <p>{value.title}</p>
                <p>
                  {value.link_address} - {value.date}
                </p>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  )
}

export default CarouselEdukasi
