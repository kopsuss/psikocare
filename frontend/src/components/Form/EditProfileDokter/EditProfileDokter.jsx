import React from "react"
import "./editProfileDokter.css"
import Image from "next/image"
import { FaUserAlt } from "react-icons/fa"

const EditProfileDokter = ({
  dataUser,
  handleEdit,
  selectedGender,
  handleGenderChange,
}) => {
  return (
    <form className="ediDokter" onSubmit={handleEdit}>
      <div className="ediDokter_file">
        <div className="border-2 shadow-sm ediDokter_img">
          {dataUser?.image_url ? (
            <Image
              src={`/upload/user/${dataUser?.image_url}`}
              alt="docter"
              width={100}
              height={100}
            />
          ) : (
            <FaUserAlt className="w-full h-full" />
          )}
        </div>

        <input
          type="file"
          name="image"
          className="file-input file-input-bordered w-full max-w-xs"
        />
      </div>

      <div className="flex flex-col gap-5">
        <div className="form_item">
          <label>Nama Lengkap</label>
          <input
            type="text"
            name="name"
            defaultValue={dataUser?.name}
            className="input input-bordered w-full"
          />
        </div>
        <div className="form_item">
          <label>Email</label>
          <input
            type="text"
            name="email"
            defaultValue={dataUser?.email}
            className="input input-bordered w-full"
          />
        </div>
        <div className="form_item">
          <label>Alamat</label>
          <input
            type="text"
            name="alamat"
            defaultValue={dataUser?.alamat}
            className="input input-bordered w-full"
          />
        </div>
        <div className="form_item_select">
          <p>Jenis Kelamin</p>
          <div className="form_item_select_content flex items-center gap-10">
            <div className="flex items-center gap-1">
              <input
                type="radio"
                name="gender"
                checked={selectedGender === "Laki - laki"}
                onChange={handleGenderChange}
                value="Laki - laki"
              />
              <label>Laki - laki</label>
            </div>
            <div className="flex items-center gap-1">
              <input
                type="radio"
                name="gender"
                checked={selectedGender === "Perempuan"}
                onChange={handleGenderChange}
                value={"Perempuan"}
              />
              <label>Perempuan</label>
            </div>
          </div>
        </div>
        <button type="submit" className="btn_edit">
          Simpan
        </button>
      </div>
    </form>
  )
}

export default EditProfileDokter
