"use client"

import React from "react"
import "./formRegister.css"
import Image from "next/image"
import banerRegister from "@/../public/assets/baner_register.svg"

const FormRegister = ({
  handleRegister,
  password,
  setPassword,
  confirmPassword,
  setConfirmPassword,
  handleTanggalChange,
  handleBulanChange,
  handleTahunChange,
}) => {
  return (
    <div className="register">
      <div className="register_form shadow-md">
        <p className="register_form_title">Register</p>
        <p>Pengguna baru, Silahkan mendaftar untuk mengakses forum PsikoCare</p>
        <form onSubmit={handleRegister}>
          <select
            className="select select-bordered w-full"
            name="role"
            required
            defaultValue={""}
          >
            <option value={""} disabled>
              Daftar Sebagai ?
            </option>
            <option value={"1"}>Dokter</option>
            <option value={"2"}>Pasien</option>
          </select>
          <input
            required
            type="text"
            placeholder="nama"
            className="input  input-bordered w-full"
            name="name"
          />
          <input
            required
            type="email"
            placeholder="email"
            className="input  input-bordered w-full"
            name="email"
          />
          <input
            required
            type="password"
            placeholder="password"
            className="input  input-bordered w-full"
            name="password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <input
            required
            type="password"
            placeholder="confirm password"
            className="input  input-bordered w-full"
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
          {password !== confirmPassword && (
            <p className="text-sm pt-1 text-red-600">
              Password doesn't match !
            </p>
          )}
          <input
            required
            type="text"
            placeholder="alamat"
            className="input  input-bordered w-full"
            name="alamat"
          />
          <div className="flex items-center justify-between section_jk">
            <p>Jenis Kelamin</p>
            <div className="flex items-center gap-3">
              <div className="flex items-center gap-1">
                <input
                  required
                  type="radio"
                  name="gender"
                  value="Laki - laki"
                />
                <label>Laki - laki</label>
              </div>
              <div className="flex items-center gap-1">
                <input required type="radio" name="gender" value="Perempuan" />
                <label>Perempuan</label>
              </div>
            </div>
          </div>

          <button type="submit" className="btn btn_register">
            Register
          </button>
        </form>
      </div>
      <div className="register_img">
        <Image src={banerRegister} alt="" width={0} height={0} />
      </div>
    </div>
  )
}

export default FormRegister
