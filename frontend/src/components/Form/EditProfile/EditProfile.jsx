import React from "react"
import "./editProfile.css"
import Image from "next/image"
import { FaUserAlt } from "react-icons/fa"

const EditProfile = ({
  dataUser,
  handleEdit,
  selectedGender,
  handleGenderChange,
}) => {
  return (
    <div className="editProfile">
      <form
        onSubmit={handleEdit}
        className="xl:w-[45%] mx-auto my-10 flex flex-col gap-5 w-[70%]"
      >
        <div className="editProfile_img">
          <div
            className="border-2 overflow-hidden p-3 shadow-sm"
            style={{ width: "150px", height: "150px", borderRadius: "100%" }}
          >
            {dataUser?.image_url ? (
              <Image
                src={`/upload/user/${dataUser?.image_url}`}
                alt="docter"
                width={1000}
                height={1000}
              />
            ) : (
              <FaUserAlt className="w-full h-full" />
            )}
          </div>

          <input
            type="file"
            name="image"
            className="file-input file-input-bordered w-full max-w-xs"
          />
        </div>
        <div className=" w-full">
          <label>Nama</label>
          <input
            name="name"
            type="text"
            placeholder="nama"
            defaultValue={dataUser?.name}
            className="input  input-bordered w-full"
          />
        </div>
        <div className=" w-full">
          <label>Email</label>
          <input
            name="email"
            type="email"
            placeholder="nama"
            defaultValue={dataUser?.email}
            className="input  input-bordered w-full"
          />
        </div>
        <div className=" w-full">
          <label>Alamat</label>
          <input
            name="alamat"
            type="text"
            placeholder="nama"
            defaultValue={dataUser?.alamat}
            className="input  input-bordered w-full"
          />
        </div>
        <div className="flex flex-col xl:flex-row xl:items-center justify-between">
          <p className="mb-3 xl:mb-0">Jenis Kelamin</p>
          <div className="flex items-center gap-10">
            <div className="flex items-center gap-1">
              <input
                type="radio"
                name="gender"
                checked={selectedGender === "Laki - laki"}
                onChange={handleGenderChange}
                value="Laki - laki"
              />
              <label>Laki - laki</label>
            </div>
            <div className="flex items-center gap-1">
              <input
                type="radio"
                name="gender"
                checked={selectedGender === "Perempuan"}
                onChange={handleGenderChange}
                value="Perempuan"
              />
              <label>Perempuan</label>
            </div>
          </div>
        </div>
        <button className="btn bg-[#066283] hover:bg-[#1c4553] mt-10 w-1/2 mx-auto text-white">
          Simpan
        </button>
      </form>
    </div>
  )
}

export default EditProfile
