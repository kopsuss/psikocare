import React, { useEffect, useState } from "react"
import Image from "next/image"
import Cookies from "js-cookie"

// css
import "./dashboard.css"

// assets
import iconKonsultasi from "@/../public/assets/icon_konsultasi.svg"
import { GoDatabase } from "react-icons/go"

// api
import {
  getUserById,
  getReceiverChat,
  getPasien,
} from "../../modules/fetch/index.js"

const Dashboard = () => {
  const [dataUser, setDataUser] = useState(null)
  const [totalKonsul, setTotalKonsul] = useState([])
  const [totalPasien, setTotalPasien] = useState([])
  const id = Cookies.get("userId")

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getUserById(id)
        setDataUser(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }
    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getReceiverChat(id)
        setTotalKonsul(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }
    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getPasien()
        setTotalPasien(res.data.data)
        console.log(res)
      } catch (err) {
        console.log(err)
      }
    }
    fetchData()
  }, [])

  const uniqueSenderIds = [
    ...new Set(totalPasien.map((item) => item.sender_id)),
  ]

  return (
    <div className="dashboard">
      <p>Dashboard</p>
      <p>
        Hallo <span className="text-[#066283]"> Dr.{dataUser?.name}</span> ,
        Selamat datang diforum <span className="text-[#066283]">PsikoCare</span>
        {""} Berinteraksi dengan nyaman demi kesehatan mental yang baik
      </p>
      <div className="dashboard_content">
        <div className="dashboard_content_item">
          <p>Total Konsultasi</p>
          <p>{totalKonsul.length}</p>
          <div className="dashboard_content_item_konsultasi">
            <Image
              src={iconKonsultasi}
              alt="icon konsultasi"
              width={100}
              height={100}
            />
          </div>
        </div>
        <div className="dashboard_content_item">
          <p>Total Data Pasien</p>

          <p>{uniqueSenderIds.length}</p>
          <div className="dashboard_content_item_dataPasien">
            <GoDatabase size={70} color="black" />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Dashboard
