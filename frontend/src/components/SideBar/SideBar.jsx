"use client"

import React, { useEffect, useState } from "react"
import "./sideBar.css"
import Image from "next/image"
import Link from "next/link"
import Cookies from "js-cookie"
import { useRouter } from "next/navigation"

// assets
import logo from "@/../public/assets/logo_dashboard.svg"
import { MdDashboard } from "react-icons/md"
import { IoLogoWechat } from "react-icons/io5"
import { FaDatabase } from "react-icons/fa"
import { usePathname } from "next/navigation"
import { FaUserAlt } from "react-icons/fa"

// api
import { getUserById } from "../../modules/fetch/index.js"

const SideBar = () => {
  const [dataUser, setDataUser] = useState(null)
  const pathname = usePathname()
  const router = useRouter()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const id = Cookies.get("userId")
        const res = await getUserById(id)
        setDataUser(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }
    fetchData()
  }, [])

  const handleLogout = () => {
    Cookies.remove("token")
    Cookies.remove("role")
    Cookies.remove("name")
    Cookies.remove("userId")

    router.push("/")
  }

  return (
    <>
      <div className="sideBar">
        <div className="sideBar_logo">
          <Image src={logo} alt="logo" width={0} height={0} />
        </div>
        <div className="sideBar_content">
          <div className="sideBar_content_item">
            <div
              className={`${
                pathname === "/" ? "active" : ""
              } sideBar_content_item_dashboard`}
            >
              <MdDashboard size={24} />
              <Link href={"/"}>Dashboard</Link>
            </div>
            <div
              className={`${
                pathname === "/konsultasi" ? "active" : ""
              } sideBar_content_item_konsultasi`}
            >
              <IoLogoWechat size={24} />
              <Link href={"/konsultasi"}>Konsultasi</Link>
            </div>
            <div
              className={`${
                pathname === "/dataPasien" ? "active" : ""
              } sideBar_content_item_data`}
            >
              <FaDatabase size={24} />
              <Link href={"/dataPasien"}>Data</Link>
            </div>
          </div>
          <Link href={"/profileDokter"} className="sideBar_content_user">
            <div className="border-2 shadow-md sideBar_content_user_img">
              {dataUser?.image_url ? (
                <Image
                  src={`/upload/user/${dataUser?.image_url}`}
                  alt="dokter"
                  width={100}
                  height={100}
                />
              ) : (
                <FaUserAlt className="w-full h-full" />
              )}
            </div>
            <p>Dr.{dataUser?.name}</p>
            <p>Psikolog</p>
          </Link>
        </div>
      </div>
      <div className="navbar_dokter">
        <button onClick={handleLogout} className="btn btn-sm">
          Log out
        </button>
      </div>
    </>
  )
}

export default SideBar
