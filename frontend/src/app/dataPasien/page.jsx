"use client"

import React, { useEffect, useState } from "react"
import SideBar from "@/components/SideBar/SideBar"
import TableDataPasien from "@/components/Table/TableDataPasien/TableDataPasien"
import Cookies from "js-cookie"
import NotFound from "@/app/not-found"
import Loading from "@/app/loading"

// api
import { getPasien } from "@/modules/fetch/index"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [dataPasien, setDataPasien] = useState([])
  const [searchTerm, setSearchTerm] = useState("")

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getPasien()
        setDataPasien(res.data.data)
      } catch (err) {
        console.log(err.response.message)
      }
    }
    fetchData()
  }, [])

  const handleSearchTerm = (e) => {
    setSearchTerm(e.target.value)
  }

  const filterDataPasien = dataPasien.filter((pasien) =>
    pasien.name.toLowerCase().includes(searchTerm.toLowerCase())
  )

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  if (userLogin === "") {
    return <Loading />
  }

  if (userLogin === "1") {
    return (
      <>
        <SideBar />
        <TableDataPasien
          handleSearchTerm={handleSearchTerm}
          searchTerm={searchTerm}
          filterDataPasien={filterDataPasien}
        />
      </>
    )
  } else {
    return <NotFound />
  }
}

export default page
