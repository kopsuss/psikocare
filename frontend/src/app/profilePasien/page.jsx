"use client"

import Cookies from "js-cookie"
import React, { useEffect, useState } from "react"
import Swal from "sweetalert2"
import { useRouter } from "next/navigation"

// components
import Loading from "../loading"
import NotFound from "../not-found"
import Footer from "@/components/Footer/Footer"
import Navbar from "@/components/Navbar/Navbar"
import TableRiwayatUser from "@/components/Table/TableRiwayatUser/TableRiwayatUser"
import WraperProfil from "@/components/Wrapper/WrapperProfil/WraperProfil"

// api
import {
  getUserById,
  deleteUser,
  getSenderChat,
} from "../../modules/fetch/index.js"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [dataUser, setDataUser] = useState(null)
  const [riwayat, setRiwayat] = useState([])
  const router = useRouter()
  const id = Cookies.get("userId")

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getUserById(id)
        setDataUser(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }

    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getSenderChat(id)
        setRiwayat(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }

    fetchData()
  }, [])

  const handleDelete = async () => {
    try {
      Swal.fire({
        title: `Are you sure delete ${dataUser?.name}`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "yes, delete",
      }).then(async () => {
        await deleteUser(id)

        Cookies.remove("token")
        Cookies.remove("role")
        Cookies.remove("name")
        Cookies.remove("userId")

        router.push("/")
      })
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  if (userLogin === "") {
    return <Loading />
  }

  if (userLogin === "2") {
    return (
      <>
        <Navbar />
        <WraperProfil dataUser={dataUser} handleDelete={handleDelete} />
        <TableRiwayatUser riwayat={riwayat} />
        <Footer />
      </>
    )
  } else {
    return <NotFound />
  }
}

export default page
