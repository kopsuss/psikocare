"use client"

import WraperEdukasi from "@/components/Wrapper/WrapperEdukasi/WraperEdukasi"
import React, { useEffect, useState } from "react"
import SectionEdukasi from "./SectionEdukasi"
import CarouselEdukasi from "@/components/Carousel/CarouselEdukasi"
import Navbar from "@/components/Navbar/Navbar"
import Footer from "@/components/Footer/Footer"
import Cookies from "js-cookie"
import NotFound from "../not-found"

const page = () => {
  const [userLogin, setUserLogin] = useState("")

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  if (userLogin === "dokter") {
    return <NotFound />
  } else {
    return (
      <>
        <Navbar />
        <WraperEdukasi />
        <SectionEdukasi />
        <CarouselEdukasi />
        <Footer />
      </>
    )
  }
}

export default page
