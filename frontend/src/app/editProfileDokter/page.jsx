"use client"

import Cookies from "js-cookie"
import React, { useEffect, useState } from "react"
import { useRouter } from "next/navigation"

// components
import Loading from "@/app/loading"
import NotFound from "@/app/not-found"
import SideBar from "@/components/SideBar/SideBar"
import EditProfileDokter from "@/components/Form/EditProfileDokter/EditProfileDokter"

// api
import {
  getUserById,
  editUser,
  uploadImage,
} from "../../modules/fetch/index.js"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [dataUser, setDataUser] = useState(null)
  const [selectedGender, setSelectedGender] = useState(dataUser?.jenis_kelamin)
  const router = useRouter()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const id = Cookies.get("userId")
        const res = await getUserById(id)
        setDataUser(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }

    fetchData()
  }, [])

  useEffect(() => {
    // Set nilai awal jenis kelamin ketika dataUser berubah
    if (dataUser) {
      setSelectedGender(dataUser?.jenis_kelamin)
    }
  }, [dataUser])

  const handleGenderChange = (event) => {
    setSelectedGender(event.target.value)
  }

  const handleEdit = async (e) => {
    e.preventDefault()

    try {
      const id = Cookies.get("userId")

      // Kirim permintaan edit user
      const res = await editUser(id, {
        name: e.target.name.value,
        email: e.target.email.value,
        alamat: e.target.alamat.value,
        jenis_kelamin: e.target.gender.value,
      })

      if (e.target.image.files[0]) {
        // Pemeriksaan file yang dipilih
        const idFile = res.data.data.id
        const formData = new FormData()
        formData.append("image_url", e.target.image.files[0])
        await uploadImage(idFile, formData)
      }

      Cookies.set("name", res.data.data.name)
      router.push("/profileDokter")
      console.log(res.data)
    } catch (err) {
      console.log(err.response)
    }
  }

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  if (userLogin === "") {
    return <Loading />
  }

  if (userLogin === "1") {
    return (
      <>
        <SideBar />
        <EditProfileDokter
          dataUser={dataUser}
          handleEdit={handleEdit}
          selectedGender={selectedGender}
          handleGenderChange={handleGenderChange}
        />
      </>
    )
  } else {
    return (
      <>
        <NotFound />
      </>
    )
  }
}

export default page
