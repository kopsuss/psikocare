"use client"

import WrapperHome from "@/components/Wrapper/WrapperHome/WrapperHome"
import SectionHome from "./SectionHome"
import Edukasi from "@/components/Carousel/CarouselEdukasi"
import Navbar from "@/components/Navbar/Navbar"
import Footer from "@/components/Footer/Footer"
import SideBar from "@/components/SideBar/SideBar"
import Cookies from "js-cookie"
import Dashboard from "@/components/Dashboard/Dashboard"
import Loading from "./loading"

export default function Home() {
  const roleLogin = Cookies.get("role")

  if (roleLogin === "") {
    return <Loading />
  }

  if (roleLogin === "1") {
    document.body.style.minWidth = "1024px"
    return (
      <>
        <SideBar />
        <Dashboard />
      </>
    )
  } else if (roleLogin === "2") {
    return (
      <>
        <Navbar />
        <WrapperHome />
        <SectionHome />
        <Edukasi />
        <Footer />
      </>
    )
  } else {
    return (
      <>
        <Navbar />
        <WrapperHome />
        <SectionHome />
        <Edukasi />
        <Footer />
      </>
    )
  }
}
