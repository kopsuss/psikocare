"use client"

import React, { useEffect, useState } from "react"
import Cookies from "js-cookie"
import SideBar from "@/components/SideBar/SideBar"
import Loading from "../loading"
import Swal from "sweetalert2"

// components
import { Konsultasi } from "../../components/konsultasi/Konsultasi"
import WraperKonsultasi from "@/components/Wrapper/WrapperKonsultasi/WraperKonsultasi"
import RecomendasiPsikolog from "./RecomendasiPsikolog"
import TextKonsultasi from "./TextKonsultasi"
import Navbar from "@/components/Navbar/Navbar"
import Footer from "@/components/Footer/Footer"

// api
import {
  getDokter,
  getPasien,
  getSenderChat,
  getReceiverChat,
  postSenderChat,
} from "@/modules/fetch/index"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [dataPsikolog, setDataPsikolog] = useState([])
  const [dataPasien, setDataPasien] = useState([])
  const [openChat, setOpenChat] = useState(false)
  const [chat, setChat] = useState([])
  const [allData, setAllData] = useState([])
  const [detailPsikolog, setDetailPsikolog] = useState([])
  const [detailPasien, setDetailPasien] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPages] = useState(0)
  const [searchTerm, setSearchTerm] = useState("")
  const size = 12

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getDokter(currentPage, size)
        setDataPsikolog(res.data.data)
        setTotalPages(res.data.totalPages)

        const allRes = await getDokter(1, res.data.totalItems)
        setAllData(allRes.data.data)
      } catch (err) {
        console.log(err)
      }
    }

    fetchData()
  }, [currentPage, size])

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getPasien()
        setDataPasien(res.data.data)
      } catch (err) {
        console.log(err)
      }
    }
    fetchData()
  }, [])

  const handleSearch = (e) => {
    setSearchTerm(e.target.value)
  }

  const paginationHandle = async (currentPage) => {
    setCurrentPage(currentPage)
  }

  const onPaginationNext = async (currentPage) => {
    setCurrentPage(currentPage + 1)
  }

  const onPaginationPrevious = async (currentPage) => {
    setCurrentPage(currentPage - 1)
  }

  const filterDataDokter = searchTerm
    ? allData.filter((user) =>
        user.name.toLowerCase().includes(searchTerm.toLowerCase())
      )
    : dataPsikolog

  const filterDataPasien = searchTerm
    ? dataPasien.filter((user) =>
        user.name.toLowerCase().includes(searchTerm.toLowerCase())
      )
    : dataPasien

  const handleOpenChat = async (id) => {
    const role = Cookies.get("role")
    const userId = Cookies.get("userId")
    if (role === "2" || role === "1") {
      // filter psikolog
      const psikologById = filterDataDokter.find((e) => e.id === id)
      const pasienById = filterDataPasien.find((e) => e.id === id)
      setDetailPsikolog(psikologById)
      setDetailPasien(pasienById)

      const sender = await getSenderChat(userId)
      const receiver = await getReceiverChat(userId)

      // Filter percakapan berdasarkan sender_id dan receiver_id
      const userChats = sender.data.data.filter(
        (chat) => chat.receiver_id === id
      )
      const psikologChats = receiver.data.data.filter(
        (chat) => chat.sender_id === id
      )

      const allChats = [...userChats, ...psikologChats]

      const sortByCreatedAt = (data) => {
        return data.sort(
          (a, b) => new Date(a.createdAt) - new Date(b.createdAt)
        )
      }

      const sortedChats = sortByCreatedAt(allChats)

      setChat(sortedChats)

      setOpenChat(!openChat)
    } else {
      Swal.fire({
        title: "Login Dulu Bro",
        icon: "warning",
      })
    }
  }

  const handleSenderChat = async (e) => {
    e.preventDefault()

    try {
      const res = await postSenderChat({
        message: e.target.chat.value,
        sender_id: Cookies.get("userId"),
        receiver_id: detailPsikolog?.id,
      })

      // Perbarui state chat setelah berhasil mengirim pesan
      setChat([...chat, res.data.data])
      e.target.chat.value = ""
    } catch (err) {
      console.log(err)
    }
  }

  const handleSenderChatDokter = async (e) => {
    e.preventDefault()

    try {
      const res = await postSenderChat({
        message: e.target.chat.value,
        sender_id: Cookies.get("userId"),
        receiver_id: detailPasien?.id,
      })

      // Perbarui state chat setelah berhasil mengirim pesan
      setChat([...chat, res.data.data])
      e.target.chat.value = ""
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  if (userLogin === "") {
    // Menampilkan tampilan loading atau placeholder
    return <Loading />
  }

  if (userLogin === "1") {
    return (
      <>
        <SideBar />
        <Konsultasi
          dataPasien={dataPasien}
          filterDataPasien={filterDataPasien}
          detailPasien={detailPasien}
          handleSearch={handleSearch}
          handleOpenChat={handleOpenChat}
          openChat={openChat}
          chat={chat}
          handleSenderChatDokter={handleSenderChatDokter}
        />
      </>
    )
  } else {
    return (
      <>
        <Navbar />
        <WraperKonsultasi />
        <RecomendasiPsikolog
          filterDataDokter={filterDataDokter}
          detailPsikolog={detailPsikolog}
          handleOpenChat={handleOpenChat}
          handleSenderChat={handleSenderChat}
          openChat={openChat}
          chat={chat}
          currentPage={currentPage}
          totalPages={totalPages}
          searchTerm={searchTerm}
          handleSearch={handleSearch}
          paginationHandle={paginationHandle}
          onPaginationNext={onPaginationNext}
          onPaginationPrevious={onPaginationPrevious}
        />
        <TextKonsultasi />
        <Footer />
      </>
    )
  }
}

export default page
