import React from "react"
import Image from "next/image"
import Cookies from "js-cookie"

// asstes
import { FaUserAlt } from "react-icons/fa"

// components
import "./konsultasi.css"

const ChatKonsultasi = ({
  openChat,
  chat,
  handleOpenChat,
  detailPsikolog,
  handleSenderChat,
}) => {
  return (
    <>
      <div className={`chatKonsultasi ${openChat ? "active" : ""}`}>
        <div className="chatKonsultasi_profile">
          <div className="border-2 chatKonsultasi_profile_img">
            {detailPsikolog?.image_url ? (
              <Image
                src={`/upload/user/${detailPsikolog?.image_url}`}
                alt=""
                width={1000}
                height={1000}
              />
            ) : (
              <FaUserAlt className="w-full h-full" />
            )}
          </div>
          <div className="chatKonsultasi_profile_desc">
            <p>Dr. {detailPsikolog?.name}</p>
            <p>{detailPsikolog?.specialist}</p>
          </div>
          <div
            onClick={handleOpenChat}
            className="absolute top-1 right-1 cursor-pointer"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="white"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
              />
            </svg>
          </div>
        </div>

        <div className="chatKonsultasi_chat">
          <div className="chatKonsultasi_chat_text">
            {chat.map((value, i) => (
              <div
                key={i}
                className={
                  value.sender_id === Cookies.get("userId")
                    ? "chatKonsultasi_chat_text_me"
                    : "chatKonsultasi_chat_text_you"
                }
              >
                <p>{value.message}</p>
              </div>
            ))}
          </div>

          <form onSubmit={handleSenderChat}>
            <div className="chatKonsultasi_chat_input">
              <input
                type="text"
                placeholder="Type here"
                name="chat"
                className="input input-sm input-bordered w-full"
              />

              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z"
                />
              </svg>
              <button type="submit">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-6 h-6"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5"
                  />
                </svg>
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}

export default ChatKonsultasi
