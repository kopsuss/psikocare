import React from "react"
import Image from "next/image"

// assets
import { FaUserAlt } from "react-icons/fa"

// components
import ChatKonsultasi from "./ChatKonsultasi"

// css
import "./konsultasi.css"

const RecomendasiPsikolog = ({
  filterDataDokter,
  handleOpenChat,
  openChat,
  chat,
  handleSenderChat,
  detailPsikolog,
  currentPage,
  totalPages,
  searchTerm,
  handleSearch,
  paginationHandle,
  onPaginationNext,
  onPaginationPrevious,
}) => {
  return (
    <>
      <div className="search">
        <input
          type="search"
          placeholder="Cari Psikolog"
          onChange={handleSearch}
        />
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className="w-6 h-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
          />
        </svg>
      </div>
      <div className="psikolog">
        <p>Konsultasi online dengan dokter siaga kami</p>
        <div className="psikolog_content">
          {filterDataDokter.map((value) => (
            <div key={value.id} className="psikolog_content_item">
              <div className="border-2 psikolog_content_item_img">
                {value.image_url ? (
                  <Image
                    src={`/upload/user/${value.image_url}`}
                    alt="img docter"
                    width={1000}
                    height={1000}
                  />
                ) : (
                  <div className="flex justify-center items-center w-full h-full">
                    <FaUserAlt className="w-1/2 h-1/2 " />
                  </div>
                )}
              </div>
              <div className="psikolog_content_item_desc">
                <p>Dr. {value.name}</p>
                <p>Psikiater ahli</p>
                <div className="psikolog_content_item_desc_place">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                    />
                  </svg>
                  <p>{value.alamat}</p>
                </div>
                <div
                  onClick={() => handleOpenChat(value.id)}
                  className="psikolog_content_item_desc_action"
                >
                  <button>Konsultasi</button>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className="mt-20 flex justify-center">
          {currentPage !== 1 && !searchTerm && (
            <button
              className="btn"
              onClick={() => onPaginationPrevious(currentPage)}
            >
              &laquo;
            </button>
          )}

          <div className="">
            {!searchTerm && (
              <>
                {currentPage > 1 && (
                  <button
                    key={currentPage - 1}
                    className="join-item btn btn-outline btn-default"
                    onClick={() =>
                      paginationHandle(currentPage - 1, totalPages)
                    }
                  >
                    {currentPage - 1}
                  </button>
                )}
                <button
                  key={currentPage}
                  className="join-item btn btn-outline btn-default btn-active btn-primary"
                  onClick={() => paginationHandle(currentPage, totalPages)}
                >
                  {currentPage}
                </button>
                {currentPage !== totalPages && (
                  <button
                    key={currentPage + 1}
                    className="join-item btn btn-outline btn-default"
                    onClick={() =>
                      paginationHandle(currentPage + 1, totalPages)
                    }
                  >
                    {currentPage + 1}
                  </button>
                )}
              </>
            )}
          </div>

          {currentPage !== totalPages && !searchTerm && (
            <button
              className="join-item btn btn-outline btn-default"
              onClick={() => onPaginationNext(currentPage)}
            >
              &raquo;
            </button>
          )}
        </div>

        <ChatKonsultasi
          openChat={openChat}
          chat={chat}
          handleOpenChat={handleOpenChat}
          detailPsikolog={detailPsikolog}
          handleSenderChat={handleSenderChat}
        />
      </div>
    </>
  )
}

export default RecomendasiPsikolog
