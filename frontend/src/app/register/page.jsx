"use client"

import React, { useEffect, useState } from "react"
import Cookies from "js-cookie"
import Loading from "../loading"
import Swal from "sweetalert2"
import { useRouter } from "next/navigation"

// components
import Footer from "@/components/Footer/Footer"
import FormRegister from "@/components/Form/FormRegister/FormRegister"
import Navbar from "@/components/Navbar/Navbar"
import NotFound from "../not-found"

// api
import { register } from "../../modules/fetch/index.js"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const router = useRouter()

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  const handleRegister = async (e) => {
    e.preventDefault()
    if (password !== confirmPassword) {
      return
    }

    try {
      const res = await register({
        role_id: e.target.role.value,
        name: e.target.name.value,
        email: e.target.email.value,
        alamat: e.target.alamat.value,
        password: e.target.password.value,
        jenis_kelamin: e.target.gender.value,
      })

      Cookies.set("token", res.data.data.token, { expires: 1 })
      Cookies.set("role", res.data.data.user.role_id, { expires: 1 })
      Cookies.set("name", res.data.data.user.name, { expires: 1 })
      Cookies.set("userId", res.data.data.user.id, { expires: 1 })

      router.push("/")
    } catch (err) {
      console.log(err)
      Swal.fire({
        icon: "error",
        title: err.response.data.message,
        showConfirmButton: false,
        timer: 3000,
        customClass: "swal-custom",
      })
    }
  }

  if (userLogin === "") {
    return <Loading />
  }

  if (userLogin === "dokter") {
    return <NotFound />
  } else {
    return (
      <>
        <Navbar />
        <FormRegister
          handleRegister={handleRegister}
          password={password}
          setPassword={setPassword}
          confirmPassword={confirmPassword}
          setConfirmPassword={setConfirmPassword}
        />
        <Footer />
      </>
    )
  }
}

export default page
