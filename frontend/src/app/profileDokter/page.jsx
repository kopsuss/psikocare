"use client"

import React, { useEffect, useState } from "react"
import "./user.css"
import Image from "next/image"
import Link from "next/link"
import Cookies from "js-cookie"

// components
import TableRiwayatDokter from "@/components/Table/TableRiwayatDokter/TableRiwayatDokter"
import SideBar from "@/components/SideBar/SideBar"

// assets
import { FaUserAlt } from "react-icons/fa"
import { CiEdit } from "react-icons/ci"
import NotFound from "@/app/not-found"
import Loading from "@/app/loading"

// api
import { getUserById, getSenderChat } from "../../modules/fetch/index.js"

const page = () => {
  const [userLogin, setUserLogin] = useState("")
  const [dataUser, setDataUser] = useState(null)
  const [riwayat, setRiwayat] = useState([])
  const id = Cookies.get("userId")

  useEffect(() => {
    const roleLogin = Cookies.get("role")
    setUserLogin(roleLogin)
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      const res = await getUserById(id)
      setDataUser(res.data.data)
    }
    fetchData()
  }, [])

  useEffect(() => {
    const fetcData = async () => {
      try {
        const res = await getSenderChat(id)
        setRiwayat(res.data.data)
        console.log("data riwayat", res)
      } catch (err) {
        console.log(err)
      }
    }
    fetcData()
  }, [])

  if (userLogin === "") {
    return <Loading />
  }

  if (userLogin === "1") {
    return (
      <>
        <SideBar />
        <div className="userDokter">
          <div className="userDokter_user">
            <div className="userDokter_parent">
              <div className="border-2 shadow-md border-black userDokter_user_img">
                {dataUser?.image_url ? (
                  <Image
                    src={`/upload/user/${dataUser?.image_url}`}
                    alt="dokter"
                    width={100}
                    height={100}
                  />
                ) : (
                  <FaUserAlt className="w-full h-full" />
                )}
              </div>
              <div className="userDokter_user_desc">
                <p>{dataUser?.name}</p>
                <p>{dataUser?.email}</p>
                <div className="userDokter_user_desc_address">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M15 10.5a3 3 0 11-6 0 3 3 0 016 0z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z"
                    />
                  </svg>
                  <p>{dataUser?.alamat}</p>
                </div>
              </div>
            </div>
            <div className="userDokter_user_action">
              <CiEdit color="white" />
              <Link href={"/editProfileDokter"}>Edit Profile</Link>
            </div>
          </div>
          <div className="userDokter_riwayat">
            <TableRiwayatDokter riwayat={riwayat} />
          </div>
        </div>
      </>
    )
  } else {
    return <NotFound />
  }
}

export default page
